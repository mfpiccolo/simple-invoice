# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
SimpleInvoice::Application.config.secret_key_base = 'e2cfaf889a6f1bdf6dc5c58573e49cfb4881a265761f913bc718c93bc78024589ff9067154b10ac08ac788cfa31f3e9b640f6d3f02b31b40e182e094e81f3abf'
