class HomeController < ApplicationController
  include Databasedotcom::OAuth2::Helpers

  def index
    @authenticated = authenticated?
    @me = me if authenticated?
    @client = client
    @client.materialize("Invoice__c")
    @invoices = Invoice__c.all
  end

  def logout
    client.logout if authenticated?
    render "index"
  end

end
